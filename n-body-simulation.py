#! /usr/bin/env python3

import numpy as np
from math import sqrt
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import sys


G = 6.67408e-11
Softening = 1e-9
N = 50
dt = 1


class Point:
    def __init__(self, position, velocity, acceleration, mass):
        self.position = position
        self.velocity = velocity
        self.acceleration = acceleration
        self.mass = mass


def read_points(filename):
    """Reads points' initial positions, velocities, accelerations and masses from a text file.
       Points are embedded in a two dimensional space.
       Returns a list of Points.
    """

    with open(filename, 'r') as file:
        lines = file.readlines()
    lines = [line.rstrip('\n').split(' ') for line in lines]
    lines = [list(map(float, line)) for line in lines]
    points = []
    for line in lines:
        position = np.array(line[:2])
        velocity = np.array(line[2:4])
        acceleration = np.array(line[4:6])
        mass = line[-1]
        points.append(Point(position, velocity, acceleration, mass))
    return points


def compute_forces(points):
    """Computes the forces between the points and updates the acceleration vectors for the points list.

       The force that particle 1 imposes on particle 2:
       F_12 = G * m1 * m2 / |r| ** 3 * r

       The force that is associated with particle 1:
       F_1 = m1 * a1 = -F_12

       Acceleration of the particle 1:
       a1 = F1 / m1 = -F_12 / m1 = - G * m2 / |r| ** 3 * r

       Softening factor is used to prevent the acceleration from going to infinity when the distance between the particles is very small.
    """

    for i, p1 in enumerate(points):
        acceleration = np.array([0, 0])
        for j, p2 in enumerate(points):
            if i != j:
                r = p1.position - p2.position
                r_norm = np.linalg.norm(r)
                r_norm_cubed = r_norm ** 3
                acceleration = acceleration - (G * p2.mass) / (r_norm_cubed + Softening) * r
        p1.acceleration = acceleration


def integrate_system(points, dt):
    """Integrates the system of points. Basic Euler method is used.
       a = dv/dt
       v = dr/dt
    """
    for point in points:
        point.velocity = point.velocity + point.acceleration * dt
        point.position = point.position + point.velocity * dt


def plot_system(points):
    x = [point.position[0] for point in points]
    y = [point.position[1] for point in points]
    plt.scatter(x, y)
    plt.xlim([-11, 11])
    plt.ylim([-11, 11])


if __name__ == "__main__":
    if len(sys.argv) == 1:
        print("Input file missing.")
    elif len(sys.argv) != 2:
        print("Wrong number of parameters.")
    else:
        filename = sys.argv[-1]
        points = read_points(filename)
        plt.xlim([-11, 11])
        plt.ylim([-11, 11])
        for i in range(N):
            plot_system(points)
            plt.pause(0.05)
            compute_forces(points)
            integrate_system(points, dt)
            plt.cla()
        plt.show()
